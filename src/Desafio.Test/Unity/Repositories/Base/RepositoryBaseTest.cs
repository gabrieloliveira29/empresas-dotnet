using System;
using Desafio.DataAccess.Context;
using Desafio.Domain.Entities;
using Desafio.Domain.Entities.Abstracts;
using Desafio.Domain.Interfaces.Repositories.Base;
using Desafio.Test.Unity.Fixtures;
using FluentAssertions;
using Xunit;

namespace Desafio.Test.Unity.Repositories.Base
{
    public class RepositoryBaseTest
    {
        private readonly DesafioContext context;
        private readonly UsuarioFixture usuarioFixture;
        private readonly RepositoryBase<Usuario> repositoryBase;
        private readonly Guid USUARIO_ID = Guid.NewGuid();
        public RepositoryBaseTest()
        {
            context = DesafioContextTestsFixture.CriarContext(nameof(RepositoryBaseTest));
            repositoryBase = new RepositoryBase<Usuario>(context);
            usuarioFixture = new UsuarioFixture();
        }

        [Fact]
        public async void DeveObterPorId()
        {
            // Arrange
            ArrangeDbSetUsuarios();

            // Act
            var resultado = await repositoryBase.ObterPorId(USUARIO_ID);

            // Assert
            resultado.Should().NotBeNull();
            resultado.Id.Should().Equals(USUARIO_ID);
        }

        private void ArrangeDbSetUsuarios()
        {
            for (byte pos = 0; pos <= 9; pos++)
            {
                context.AddRange(usuarioFixture.GerarUsuarioValido());
            }

            context.AddRange(usuarioFixture.GerarUsuarioValido(USUARIO_ID));

            context.SaveChanges();
        }
    }
}