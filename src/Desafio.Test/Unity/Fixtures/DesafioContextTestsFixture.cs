using Desafio.DataAccess.Context;
using Microsoft.EntityFrameworkCore;

namespace Desafio.Test.Unity.Fixtures
{
    public static class DesafioContextTestsFixture
    {
         public static DesafioContext CriarContext(string databaseName)
        {
            var builder = new DbContextOptionsBuilder<DesafioContext>();
            builder.UseInMemoryDatabase(databaseName);

            var context = new DesafioContext(builder.Options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            return context;
        }
    }
}