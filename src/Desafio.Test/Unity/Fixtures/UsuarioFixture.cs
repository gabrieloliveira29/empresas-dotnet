using System;
using Bogus;
using Desafio.Domain.Entities;

namespace Desafio.Test.Unity.Fixtures
{
    public class UsuarioFixture
    {
         private const string LOCALE = "pt_BR";

        public Usuario GerarUsuarioValido()
        {
            var usuario = new Faker<Usuario>(LOCALE)
                        .RuleFor(usuario => usuario.Id, fake => fake.Random.Guid())
                        .RuleFor(usuario => usuario.Nome, fake => fake.Person.FullName)
                        .RuleFor(usuario => usuario.Login, fake => fake.Person.UserName)
                        .RuleFor(usuario => usuario.Senha, fake => fake.Random.String())
                        .RuleFor(usuario => usuario.Cadastro, fake => fake.Person.DateOfBirth)
                        .RuleFor(usuario => usuario.Administrador, fake => fake.Random.Bool())
                        .Generate();

            return usuario;
        }

        public Usuario GerarUsuarioValido(Guid id)
        {
            var usuario = new Faker<Usuario>(LOCALE)
                        .RuleFor(usuario => usuario.Id, id)
                        .RuleFor(usuario => usuario.Nome, fake => fake.Person.FullName)
                        .RuleFor(usuario => usuario.Login, fake => fake.Person.UserName)
                        .RuleFor(usuario => usuario.Senha, fake => fake.Random.String())
                        .RuleFor(usuario => usuario.Cadastro, fake => fake.Person.DateOfBirth)
                        .RuleFor(usuario => usuario.Administrador, fake => fake.Random.Bool())
                        .Generate();

            return usuario;
        }
    }
}