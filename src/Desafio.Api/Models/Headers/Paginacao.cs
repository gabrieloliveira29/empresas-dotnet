﻿using System.Text.Json;

namespace Desafio.Api.Models.Headers
{
    public class Paginacao
    {
        public Paginacao(int pagina, int porPagina, int totalItens)
        {
            Pagina = pagina;
            PorPagina = porPagina;
            TotalItens = totalItens;
            
        }

        public int Pagina { get; private set; }
        public int PorPagina { get; private set; }
        public int TotalItens { get; private set; }

        public override string ToString() => JsonSerializer.Serialize(this);


    }
}
