﻿namespace Desafio.Api.Models.Request
{
    public class GeneroPost
    {
        public GeneroPost(string descricao)
           => (Descricao) = (descricao);

        public string Descricao { get; set; }      
    }
}
