namespace Desafio.Api.Models.Request
{
    public class UsuarioPost
    {
        public UsuarioPost(string nome, string login, string senha, bool administrador)
        => (Nome, Login, Senha, Administrador) = (nome, login, senha, administrador);

        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public bool Administrador { get; set; }
    }
}