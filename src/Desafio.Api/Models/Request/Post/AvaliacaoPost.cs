﻿using System;

namespace Desafio.Api.Models.Request
{
    public class AvaliacaoPost
    {
       public AvaliacaoPost(int nota)
         => (Nota) = (nota);
         
        public int Nota { get; set; }
    }
}
