namespace Desafio.Api.Models.Request
{
    public class LoginPost
    {
        public LoginPost(string login, string senha)
        => (Login, Senha) = (login, senha);

        public string Login { get; set; }
        public string Senha { get; set; }
    }
}