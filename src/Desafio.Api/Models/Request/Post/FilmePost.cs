﻿using System;
using System.Collections.Generic;

namespace Desafio.Api.Models.Request
{
    public class FilmePost
    {
        public FilmePost(string nome, string descricao, int ano, IEnumerable<Guid> diretoresIds, IEnumerable<Guid> atoresIds)
           => (Nome, Descricao, Ano, DiretoresIds, AtoresIds) = (nome, descricao, ano, diretoresIds, atoresIds);
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int Ano { get; set; }
        public IEnumerable<Guid> DiretoresIds { get; set; }
        public IEnumerable<Guid> AtoresIds { get; set; }

    }
}
