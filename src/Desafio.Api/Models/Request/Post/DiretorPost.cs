﻿using System;

namespace Desafio.Api.Models.Request
{
    public class DiretorPost
    {
       public DiretorPost(string nome, string sobrenome)
         =>(Nome, Sobrenome) = (nome, sobrenome);
         
        public string Nome { get; set; }
        public string Sobrenome { get; set; }   
    }
}
