﻿using System;

namespace Desafio.Api.Models.Request
{
    public class Avaliacao
    {
       public Avaliacao(int nota, Guid usuarioId, Guid filmeId)
         => (Nota, UsuarioId, FilmeId) = (nota, usuarioId, filmeId);
         
        public int Nota { get; private set; }
        public Guid UsuarioId { get; private set; }
        public Guid FilmeId { get; private set; }
    }
}
