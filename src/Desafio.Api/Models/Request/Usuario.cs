namespace Desafio.Api.Models.Request
{
    public class Usuario
    {
        public Usuario(string nome, string login, string senha, bool administrador)
        => (Nome, Login, Senha, Administrador) = (nome, login, senha, administrador);
        public string Nome { get; private set; }
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public bool Administrador { get; private set; }
    }
}