﻿using System;

namespace Desafio.Api.Models.Request
{
    public class Ator
    {
       public Ator(string nome, string sobrenome)
         =>(Nome, Sobrenome) = (nome, sobrenome);
         
        public Guid Id { get;  set; }
        public string Nome { get;  set; }
        public string Sobrenome { get;  set; }   
    }
}
