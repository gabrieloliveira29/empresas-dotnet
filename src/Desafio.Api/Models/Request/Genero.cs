﻿namespace Desafio.Api.Models.Request
{
    public class Genero
    {
        public Genero(string descricao)
           => (Descricao) = (descricao);

        public string Descricao { get; private set; }      
    }
}
