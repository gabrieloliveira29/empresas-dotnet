﻿using System;

namespace Desafio.Api.Models.Request
{
    public class Diretor
    {
       public Diretor(string nome, string sobrenome)
         =>(Nome, Sobrenome) = (nome, sobrenome);
         
        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public string Sobrenome { get; private set; }   
    }
}
