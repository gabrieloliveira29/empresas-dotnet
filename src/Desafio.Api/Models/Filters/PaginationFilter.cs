﻿namespace Desafio.Api.Models.Filters
{
    public class PaginationFilter
    {
        public PaginationFilter()
        {
            this.Pagina = 1;
            this.PorPagina = 10;
        }
        public PaginationFilter(int pagina, int porPagina)
        {
            this.Pagina = pagina < 1 ? 1 : pagina;
            this.PorPagina = porPagina < 1 ? 10 : porPagina;
        }

        public int Pagina { get; set; }
        public int PorPagina { get; set; }
    }
}
