using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Desafio.CrossCutting.Notification;
using System.Net;
using Microsoft.AspNetCore.Http;
using System.Text.Json;

namespace Desafio.Api.Filters
{
    public class NotificationFilter : IAsyncResultFilter
    {
        private readonly INotificationContext notificationContext;
        public NotificationFilter(INotificationContext notificationContext)
        {
            this.notificationContext = notificationContext;
        }

        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (notificationContext.HasNotifications)
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.HttpContext.Response.ContentType = "application/json";

                var notifications = JsonSerializer.Serialize(notificationContext.Notifications);
                await context.HttpContext.Response.WriteAsync(notifications);

                return;
            }

            await next();
        }
    }
}