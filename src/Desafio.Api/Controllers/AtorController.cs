﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio.Api.Models.Filters;
using Desafio.Api.Models.Headers;
using Desafio.Api.Models.Request;
using Desafio.Application.Commands;
using Desafio.Application.Queries.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Desafio.Api.Controllers
{
    [ApiController]
    public class AtorController : ControllerBase
    {

        private readonly IMediator mediator;
        private readonly IAtorQuery atorQuery;

        public AtorController(IAtorQuery atorQuery,
                              IMediator mediator)
        {
            this.atorQuery = atorQuery;
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("ator")]
        [Authorize(Roles = "adm")]
        public async Task<ActionResult<Guid>> Cadastrar([FromBody] AtorPost ator)
        {
            var command = new CadastrarAtorCommand(ator.Nome, ator.Sobrenome);
            var id = await mediator.Send(command);

            return id;
        }

        [HttpGet]
        [Route("atores")]
        [Authorize]
        public ICollection<Domain.Entities.Ator> Listar([FromQuery] PaginationFilter filtro)
        {
            filtro = new PaginationFilter(filtro.Pagina, filtro.PorPagina);

            var resultado = atorQuery.Listar(filtro.Pagina, filtro.PorPagina);

            var paginacao = new Paginacao(resultado.PageNumber, resultado.PageSize, resultado.PageCount);
            Response.Headers.Add("pagination", paginacao.ToString()) ;

            return resultado.ToList();
        }
    }
}
