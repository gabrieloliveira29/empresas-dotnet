﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio.Api.Models.Filters;
using Desafio.Api.Models.Headers;
using Desafio.Api.Models.Request;
using Desafio.Application.Commands;
using Desafio.Application.Queries.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Desafio.Api.Controllers
{
    [ApiController]
    public class DiretorController : ControllerBase
    {

        private readonly IMediator mediator;
        private readonly IDiretorQuery diretorQuery;

        public DiretorController(IDiretorQuery diretorQuery,
                                 IMediator mediator)
        {
            this.diretorQuery = diretorQuery;
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("diretor")]
        [Authorize(Roles = "adm")]
        public async Task<ActionResult<Guid>> Cadastrar([FromBody] DiretorPost diretor)
        {
            var command = new CadastrarDiretorCommand(diretor.Nome, diretor.Sobrenome);
            var id = await mediator.Send(command);

            return id;
        }

        [HttpGet]
        [Route("diretores")]
        [Authorize]
        public ICollection<Domain.Entities.Diretor> Listar([FromQuery] PaginationFilter filtro)
        {
            filtro = new PaginationFilter(filtro.Pagina, filtro.PorPagina);

            var resultado = diretorQuery.Listar(filtro.Pagina, filtro.PorPagina);

            var paginacao = new Paginacao(resultado.PageNumber, resultado.PageSize, resultado.PageCount);
            Response.Headers.Add("pagination", paginacao.ToString());

            return resultado.ToList();
        }
    }
}
