﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio.Api.Models.Filters;
using Desafio.Api.Models.Headers;
using Desafio.Api.Models.Request;
using Desafio.Application.Commands;
using Desafio.Application.Queries.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Desafio.Api.Controllers
{
    [ApiController]
    public class FilmeController : ControllerBase
    {

        private readonly IMediator mediator;
        private readonly IFilmeQuery filmeQuery;

        public FilmeController(IFilmeQuery filmeQuery,
                               IMediator mediator)
        {
            this.filmeQuery = filmeQuery;
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("filme")]
        [Authorize(Roles = "adm")]
        public async Task<ActionResult<Guid>> Cadastrar([FromBody] FilmePost filme)
        {
            var command = new CadastrarFilmeCommand(filme.Nome, filme.Descricao, filme.Ano, filme.DiretoresIds, filme.AtoresIds);
            var id = await mediator.Send(command);

            return id;
        }

        [HttpPost]
        [Route("filme/{id}/avaliacao")]
        [Authorize(Roles = "user")]
        public async Task<ActionResult<Guid>> Cadastrar(Guid id, [FromBody] AvaliacaoPost avaliacao)
        {
            var idUsuarioLogado = Guid.Parse(User.Identity.Name);

            var command = new CadastrarAvaliacaoCommand(avaliacao.Nota, idUsuarioLogado, avaliacao.id);
            var idAvaliacao = await mediator.Send(command);

            return idAvaliacao;
        }

        [HttpGet]
        [Route("filmes")]
        [Authorize]
        public ICollection<Domain.Entities.Filme> Listar([FromQuery] PaginationFilter filtro)
        {
            filtro = new PaginationFilter(filtro.Pagina, filtro.PorPagina);

            var resultado = filmeQuery.Listar(filtro.Pagina, filtro.PorPagina);

            var paginacao = new Paginacao(resultado.PageNumber, resultado.PageSize, resultado.PageCount);
            Response.Headers.Add("pagination", paginacao.ToString());

            return resultado.ToList();
        }
    }
}
