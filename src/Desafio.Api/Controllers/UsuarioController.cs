﻿using System;
using System.Threading.Tasks;
using Desafio.Api.Jwt;
using Desafio.Api.Models.Request;
using Desafio.Application.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;


namespace Desafio.Api.Controllers
{
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IMediator mediator;

        public UsuarioController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<dynamic>> Cadastrar([FromBody] LoginPost login)
        {
            var command = new LogarUsuarioCommand(login.Login, login.Senha);
            var usuario = await mediator.Send(command);
           
            if (usuario == null)
                return null;

            var token = TokenService.GenerateToken(usuario);
            return new
            {
                usuario = usuario,
                token = token
            };
        }

        [HttpPost]
        [Route("usuario")]
        public async Task<ActionResult<Guid>> Cadastrar([FromBody] UsuarioPost usuario)
        {
            var command = new CadastrarUsuarioCommand(usuario.Nome, usuario.Login, usuario.Senha, usuario.Administrador);
            var id = await mediator.Send(command);

            return id;
        }


        [HttpPatch]
        [Route("usuario/{id}/ativar")]
        public async Task<ActionResult> Ativar(Guid id)
        {
            var command = new AtivarUsuarioCommand(id);
            await mediator.Send(command);

            return Ok();
        }

        [HttpPatch]
        [Route("usuario/{id}/desativar")]
        public async Task<ActionResult> Desativar(Guid id)
        {
            var command = new DesativarUsuarioCommand(id);
            await mediator.Send(command);

            return Ok();
        }
    }
}
