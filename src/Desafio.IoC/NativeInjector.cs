using Desafio.Application.Queries;
using Desafio.Application.Queries.Interfaces;
using Desafio.CrossCutting.Notification;
using Desafio.DataAccess.Context;
using Desafio.DataAccess.Repositories;
using Desafio.Domain.Interfaces.Repositories;
using Desafio.Domain.Interfaces.Services;
using Desafio.Domain.Services;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Desafio.IoC
{
    public class NativeInjector
    {
        public static void RegisterServices(IServiceCollection service)
        {
            service.AddScoped<DesafioContext>();


            service.AddScoped<IAtorRepository, AtorRepository>();
            service.AddScoped<IAvaliacaoRepository, AvaliacaoRepository>();
            service.AddScoped<IDiretorRepository, DiretorRepository>();
            service.AddScoped<IFilmeRepository, FilmeRepository>();
            service.AddScoped<IGeneroRepository, GeneroRepository>();
            service.AddScoped<IUsuarioRepository, UsuarioRepository>();

            service.AddScoped<IAtorQuery, AtorQuery>();
            service.AddScoped<IDiretorQuery, DiretorQuery>();
            service.AddScoped<IFilmeQuery, FilmeQuery>();
            service.AddScoped<IGeneroQuery, GeneroQuery>();
            service.AddScoped<IUsuarioQuery, UsuarioQuery>();


            service.AddScoped<IGeneroService, GeneroService>();
            
            service.AddScoped<INotificationContext, NotificationContext>();
        }
    }
}