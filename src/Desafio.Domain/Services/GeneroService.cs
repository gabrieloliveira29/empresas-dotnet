using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using Desafio.Domain.Interfaces.Services;
using System.Threading.Tasks;

namespace Desafio.Domain.Services
{
    public class GeneroService : IGeneroService
    {
        private readonly IGeneroRepository repositoryGenero;

        public GeneroService(IGeneroRepository repositoryGenero)
        {
            this.repositoryGenero = repositoryGenero;
        }

        public void Inserir(Genero genero)
        {
            repositoryGenero.Inserir(genero);
        }

        public async Task<bool> Salvar()
        {
            return await repositoryGenero.Salvar();
        }
    }
}