using Desafio.Domain.Entities.Abstracts;
using Desafio.Domain.Validators;
using System;
using System.Collections.Generic;

namespace Desafio.Domain.Entities
{
    public class Usuario : Entity
    {
        public Usuario(string nome, string login, string senha, bool administrador)
        {
            Nome = nome;
            Login = login;
            Senha = senha;
            Administrador = administrador;
            Ativo = false;
            Cadastro = DateTime.Now;
            Validar(this, new UsuarioValidator());
            avaliacoes = new List<Avaliacao>();
        }

        public string Nome { get; private set; }
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public bool Ativo { get; private set; }
        public bool Administrador { get; private set; }
        public DateTime Cadastro { get; private set; }
        private List<Avaliacao> avaliacoes;
        public IEnumerable<Avaliacao> Avaliacoes => avaliacoes.AsReadOnly();

        public void AtualizarDados(string nome, string login)
        => (Nome, Login) = (nome, login);
        public void Ativar() => Ativo = true;
        public void Desativar() => Ativo = false;
    }
}