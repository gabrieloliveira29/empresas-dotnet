using Desafio.Domain.Entities.Abstracts;
using Desafio.Domain.Validators;
using System;

namespace Desafio.Domain.Entities
{
    public class Avaliacao : Entity
    {
        public Avaliacao(int nota, Guid usuarioId, Guid filmeId)
        {
            Nota = nota;
            UsuarioId = usuarioId;
            FilmeId = filmeId;
            Validar(this, new AvaliacaoValidator());
        }

        public int Nota { get; private set; }
        public Guid UsuarioId { get; private set; }
        public Guid FilmeId { get; private set; }
        public Usuario Usuario { get; private set; }
        public Filme Filme { get; private set; }
    }
}