using Desafio.Domain.Entities.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Desafio.Domain.Entities
{
    public class Filme : Entity
    {
        public Filme(string nome, string descricao, int ano)
        {
            Nome = nome;
            Descricao = descricao;
            Ano = ano;
            diretores = new List<FilmeDiretor>();
            atores = new List<FilmeAtor>();
            generos = new List<FilmeGenero>();
            avaliacoes = new List<Avaliacao>();
        }

        public string Nome { get; private set; }
        public string Descricao { get; private set; }
        public int Ano { get; private set; }
        public int NotaAvaliacao => avaliacoes.Sum(avaliacao => avaliacao.Nota);

        private List<FilmeDiretor> diretores;
        public IEnumerable<FilmeDiretor> Diretores => diretores.AsReadOnly();
        private List<FilmeAtor> atores;
        public IEnumerable<FilmeAtor> Atores => atores.AsReadOnly();
        private List<FilmeGenero> generos;
        public IEnumerable<FilmeGenero> Generos => generos.AsReadOnly();
        private List<Avaliacao> avaliacoes;
        public IEnumerable<Avaliacao> Avaliacoes => avaliacoes.AsReadOnly();

        public void AdicionarAtores(IEnumerable<Guid> atoresIds)
         => atores.AddRange(atoresIds.Select(atorId => new FilmeAtor(Id, atorId)));

        public void AdicionarDiretores(IEnumerable<Guid> diretoresIds)
         => diretores.AddRange(diretoresIds.Select(diretorId => new FilmeDiretor(Id, diretorId)));
    }
}