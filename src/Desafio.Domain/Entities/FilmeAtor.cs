using System;

namespace Desafio.Domain.Entities
{
    public class FilmeAtor
    {
        public FilmeAtor(Guid filmeId, Guid atorId)
         =>(FilmeId, AtorId) = (filmeId, atorId);
        public Guid FilmeId { get; private set; }
        public Guid AtorId { get; private set; }
        public Filme Filme { get; private set; }
        public Ator Ator { get; private set; }
    }
}