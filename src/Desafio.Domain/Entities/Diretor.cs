using Desafio.Domain.Entities.Abstracts;
using Desafio.Domain.Validators;
using System.Collections.Generic;

namespace Desafio.Domain.Entities
{
    public class Diretor : Entity
    {
        public Diretor(string nome, string sobrenome)
        {
            Nome = nome;
            Sobrenome = sobrenome;
            Validar(this, new DiretorValidator());
        }

        public string Nome { get; private set; }
        public string Sobrenome { get; private set; }
        private List<FilmeDiretor> filmes;
        public IEnumerable<FilmeDiretor> Filmes => filmes.AsReadOnly();
    }
}