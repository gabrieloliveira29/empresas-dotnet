using Desafio.Domain.Entities.Abstracts;
using Desafio.Domain.Validators;
using System.Collections.Generic;

namespace Desafio.Domain.Entities
{
    public class Ator : Entity
    {
        public Ator(string nome, string sobrenome)
        {
            Nome = nome;
            Sobrenome = sobrenome;
            filmes = new List<FilmeAtor>();
            Validar(this, new AtorValidator());
        }

        public string Nome { get; private set; }
        public string Sobrenome { get; private set; }
        private List<FilmeAtor> filmes;
        public IEnumerable<FilmeAtor> Filmes => filmes.AsReadOnly();
    }
}