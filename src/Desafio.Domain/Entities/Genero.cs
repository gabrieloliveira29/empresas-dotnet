using Desafio.Domain.Entities.Abstracts;
using Desafio.Domain.Validators;
using System.Collections.Generic;

namespace Desafio.Domain.Entities
{
    public class Genero : Entity
    {
        public Genero(string descricao)
        {
            Descricao = descricao;
            Validar(this, new GeneroValidator());
        }

        public string Descricao { get; private set; }
        private List<FilmeGenero> filmes;
        public IEnumerable<FilmeGenero> Filmes => filmes.AsReadOnly();
    }
}