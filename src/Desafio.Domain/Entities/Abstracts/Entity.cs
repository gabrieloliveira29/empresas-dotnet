using System;
using FluentValidation;
using FluentValidation.Results;

namespace Desafio.Domain.Entities.Abstracts
{
    public abstract class Entity
    {
        public Guid Id { get; protected set; }
        public bool Valido { get; protected set; }
        public ValidationResult ValidationResult { get; private set; }

        public bool Validar<TEntity>(TEntity entity, AbstractValidator<TEntity> validator)
        {
            ValidationResult = validator.Validate(entity);
            return Valido = ValidationResult.IsValid;
        }
    }
}