using System;

namespace Desafio.Domain.Entities
{
    public class FilmeDiretor
    {
        public FilmeDiretor(Guid filmeId, Guid diretorId)
        => (FilmeId, DiretorId) = (filmeId, diretorId);
        public Guid FilmeId { get; private set; }
        public Guid DiretorId { get; private set; }
        public Filme Filme { get; private set; }
        public Diretor Diretor { get; private set; }
    }
}