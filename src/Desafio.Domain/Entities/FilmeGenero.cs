using System;

namespace Desafio.Domain.Entities
{
    public class FilmeGenero
    {
        public Guid FilmeId { get; private set; }
        public Guid GeneroId { get; private set; }
        public Filme Filme { get; private set; }
        public Genero Genero { get; private set; }
    }
}