using Desafio.Domain.Entities;
using FluentValidation;

namespace Desafio.Domain.Validators
{
    public class FilmeValidator : AbstractValidator<Filme>
    {
        public FilmeValidator()
        {
            RuleFor(filme => filme.Nome)
                .NotEmpty()
                .NotNull()
                .WithMessage("Nome não pode ser vazio.");
            
            RuleFor(filme => filme.Descricao)
                .NotEmpty()
                .NotNull()
                .WithMessage("Descrição não pode ser vazio.");
            
            RuleFor(filme => filme.Ano)
                .NotEmpty()
                .NotNull()
                .GreaterThanOrEqualTo(1888)
                .WithMessage("Ano não é válido.");
        }
    }
}