using Desafio.Domain.Entities;
using FluentValidation;

namespace Desafio.Domain.Validators
{
    public class DiretorValidator : AbstractValidator<Diretor>
    {
        public DiretorValidator()
        {
            RuleFor(diretor => diretor.Nome)
                .NotEmpty()
                .NotNull()
                .WithMessage("Nome não pode ser vazio.");
            
            RuleFor(diretor => diretor.Sobrenome)
                .NotEmpty()
                .NotNull()
                .WithMessage("Sobrenome não pode ser vazio.");
        }
    }
}