using Desafio.Domain.Entities;
using FluentValidation;

namespace Desafio.Domain.Validators
{
    public class UsuarioValidator : AbstractValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(usuario => usuario.Nome)
                .NotEmpty()
                .NotNull()
                .MinimumLength(3)
                .WithMessage("Nome tem que ser maior que 2 caracteres.");
            
            RuleFor(usuario => usuario.Login)
                .NotEmpty()
                .NotNull()
                .MinimumLength(4)
                .When(usuario => usuario.Id == null)
                .WithMessage("Login tem que ser maior que 3 caracteres.");
            
            RuleFor(usuario => usuario.Senha)
                .NotEmpty()
                .NotNull()
                .When(usuario => usuario.Id == null)
                .WithMessage("Senha não pode ser vazia.");
        }
    }
}