using Desafio.Domain.Entities;
using FluentValidation;

namespace Desafio.Domain.Validators
{
    public class GeneroValidator : AbstractValidator<Genero>
    {
        public GeneroValidator()
        {
            RuleFor(genero => genero.Descricao)
                .NotEmpty()
                .EmailAddress()
                .WithMessage("Descrição não pode ser vazio.");
        }
    }
}