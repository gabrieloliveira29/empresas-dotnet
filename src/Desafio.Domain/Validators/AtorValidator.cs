using Desafio.Domain.Entities;
using FluentValidation;

namespace Desafio.Domain.Validators
{
    public class AtorValidator : AbstractValidator<Ator>
    {
        public AtorValidator()
        {
            RuleFor(ator => ator.Nome)
                .NotEmpty()
                .WithMessage("Nome não pode ser vazio.")
                .NotNull()
                .WithMessage("Nome não pode ser nulo.");
            
            RuleFor(ator => ator.Sobrenome)
                .NotEmpty()
                .NotNull()
                .WithMessage("Sobrenome não pode ser vazio.");
        }
    }
}