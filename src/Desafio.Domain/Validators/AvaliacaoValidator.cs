using Desafio.Domain.Entities;
using FluentValidation;

namespace Desafio.Domain.Validators
{
    public class AvaliacaoValidator : AbstractValidator<Avaliacao>
    {
        public AvaliacaoValidator()
        {
            RuleFor(avaliacao => avaliacao.Nota)
                .NotEmpty()
                .WithMessage("Nota deve ser informada.")
                .NotNull()
                .WithMessage("Nota deve ser informada.")
                .InclusiveBetween(0, 4)
                .WithMessage("Nota deve estar entre 0 e 4.");

            RuleFor(avaliacao => avaliacao.UsuarioId)
                .NotEmpty()
                .WithMessage("Usuário deve ser informado.")
                .NotNull()
                .WithMessage("Usuário deve ser informado.");

            RuleFor(avaliacao => avaliacao.Filme)
                .NotEmpty()
                .WithMessage("Filme deve ser informado.")
                .NotNull()
                .WithMessage("Filme deve ser informado.");
        }
    }
}