using System.Threading.Tasks;
using Desafio.Domain.Entities;

namespace Desafio.Domain.Interfaces.Services
{
    public interface IGeneroService
    {
        void Inserir(Genero genero);
        Task<bool> Salvar();
    }
}