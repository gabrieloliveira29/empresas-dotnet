using Desafio.CrossCutting.Pagination;
using System;
using System.Threading.Tasks;

namespace Desafio.Domain.Interfaces.Repositories.Base
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Inserir(TEntity entidade);
        Task<TEntity> ObterPorId(Guid id);
        PagedList<TEntity> Listar(int pagina, int porPagina);
        void Atualizar(TEntity entidade);
        void Remover(TEntity entity);
        void Remover(Guid id);
        Task<bool> Salvar();
    }
}