using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories.Base;

namespace Desafio.Domain.Interfaces.Repositories
{
    public interface IAtorRepository : IRepositoryBase<Ator>
    {
    }
}