using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories.Base;
using System.Threading.Tasks;

namespace Desafio.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
        Task<Usuario> ObterPorLogin(string login);
    }
}