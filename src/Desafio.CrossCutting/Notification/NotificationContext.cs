using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace Desafio.CrossCutting.Notification
{
    public class NotificationContext : INotificationContext
    {
        public NotificationContext() => notifications = new List<Notification>();
        private readonly List<Notification> notifications;
        public IReadOnlyCollection<Notification> Notifications => notifications;
        public bool HasNotifications => notifications.Any();

        public void AddNotification(string key, string message)
         => notifications.Add(new Notification(key, message));

        public void AddNotification(Notification notification)
         => notifications.Add(notification);
        public void AddNotifications(ICollection<Notification> notifications)
         => this.notifications.AddRange(notifications);

        public void AddNotifications(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Errors)
            {
                AddNotification(error.ErrorCode, error.ErrorMessage);
            }
        }
    }
}