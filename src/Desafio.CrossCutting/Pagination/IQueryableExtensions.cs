using System.Linq;

namespace Desafio.CrossCutting.Pagination
{
    public static class IQueryableExtensions
    {

        public static PagedList<T> ToPagedList<T>(this IQueryable<T> source, int pageNumber, int pageSize)
        {
            return new PagedList<T>(source, pageNumber, pageSize);
        }

        public static PagedList<T> ToPagedList<T>(this IQueryable<T> source)
        {
            return new PagedList<T>(source);
        }
    }
}