using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Desafio.CrossCutting.Pagination
{
    public class PagedList<T> : IPagedList, IEnumerable<T>
    {
        public int PageCount => TotalItemCount > 0 ? (int)Math.Ceiling(TotalItemCount / (double)PageSize) : 0;
        public int TotalItemCount { get; private set; }
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        private List<T> source = new List<T>();
        protected IReadOnlyList<T> Source => source;

        public PagedList(IQueryable<T> source, int pageNumber, int pageSize)
        {
            TotalItemCount = source?.Count() ?? 0;
            PageNumber = pageNumber;
            PageSize = pageSize > 0 ? pageSize : TotalItemCount;

            if (TotalItemCount > 0)
            {
                AddRange(source.Skip((PageNumber - 1) * PageSize).Take(PageSize).ToList());
            }
        }

        public PagedList(IQueryable<T> source) : this(source, 1, 0) { }
        public IEnumerator<T> GetEnumerator()
        {
            return Source.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        internal void AddRange(List<T> pagedList)
        {
            source.AddRange(pagedList);
        }
    }
}