using System;
using System.Threading;
using System.Threading.Tasks;
using Desafio.Application.Commands;
using Desafio.Application.Handlers.Base;
using Desafio.CrossCutting.Notification;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using MediatR;

namespace Desafio.Application.Handlers
{
    public class GeneroCommandHandler : CommandHandler,
        IRequestHandler<CadastrarGeneroCommand, Guid>
    {
        private readonly IGeneroRepository generoRepository;
        private readonly INotificationContext notificationContext;
        public GeneroCommandHandler(IGeneroRepository generoRepository,
                                    INotificationContext notificationContext,
                                    IMediator mediator) : base(mediator)
        {
            this.generoRepository = generoRepository;
            this.notificationContext = notificationContext;
        }

        public async Task<Guid> Handle(CadastrarGeneroCommand request, CancellationToken cancellationToken)
        {
            var genero = new Genero(request.Descricao);
            
            if(!genero.Valido)
            {
                notificationContext.AddNotifications(genero.ValidationResult);
                return Guid.Empty;
            }
            generoRepository.Inserir(genero);
            await generoRepository.Salvar();
            
            return genero.Id;
        }

    }
}