using System;
using System.Threading;
using System.Threading.Tasks;
using Desafio.Application.Commands;
using Desafio.Application.Handlers.Base;
using Desafio.CrossCutting.Notification;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using MediatR;

namespace Desafio.Application.Handlers
{
    public class AtorCommandHandler : CommandHandler,
        IRequestHandler<CadastrarAtorCommand, Guid>
    {
        private readonly IAtorRepository atorRepository;
        private readonly INotificationContext notificationContext;
        public AtorCommandHandler(IAtorRepository atorRepository,
                                    INotificationContext notificationContext,
                                    IMediator mediator) : base(mediator)
        {
            this.atorRepository = atorRepository;
            this.notificationContext = notificationContext;
        }

        public async Task<Guid> Handle(CadastrarAtorCommand request, CancellationToken cancellationToken)
        {
            var ator = new Ator(request.Nome, request.Sobrenome);

            if (!ator.Valido)
            {
                notificationContext.AddNotifications(ator.ValidationResult);
                return Guid.Empty;
            }
            try
            {
                atorRepository.Inserir(ator);
                await atorRepository.Salvar();
            }catch(Exception e)
            {
                var a = e.Message;
            }
            

            return ator.Id;
        }

    }
}