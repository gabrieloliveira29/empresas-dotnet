using System;
using System.Threading;
using System.Threading.Tasks;
using Desafio.Application.Commands;
using Desafio.Application.Handlers.Base;
using Desafio.CrossCutting.Notification;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using MediatR;

namespace Desafio.Application.Handlers
{
    public class DiretorCommandHandler : CommandHandler,
        IRequestHandler<CadastrarDiretorCommand, Guid>
    {
        private readonly IDiretorRepository diretorRepository;
        private readonly INotificationContext notificationContext;
        public DiretorCommandHandler(IDiretorRepository diretorRepository,
                                    INotificationContext notificationContext,
                                    IMediator mediator) : base(mediator)
        {
            this.diretorRepository = diretorRepository;
            this.notificationContext = notificationContext;
        }

        public async Task<Guid> Handle(CadastrarDiretorCommand request, CancellationToken cancellationToken)
        {
            var diretor = new Diretor(request.Nome, request.Sobrenome);

            if (!diretor.Valido)
            {
                notificationContext.AddNotifications(diretor.ValidationResult);
                return Guid.Empty;
            }
            
            diretorRepository.Inserir(diretor);
            await diretorRepository.Salvar();

            return diretor.Id;
        }

    }
}