using System;
using System.Threading;
using System.Threading.Tasks;
using Desafio.Application.Commands;
using Desafio.Application.Handlers.Base;
using Desafio.CrossCutting.Notification;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using MediatR;

namespace Desafio.Application.Handlers
{
    public class AvaliacaoCommandHandler : CommandHandler,
        IRequestHandler<CadastrarAvaliacaoCommand, Guid>
    {
        private readonly IAvaliacaoRepository avaliacaoRepository;
        private readonly INotificationContext notificationContext;
        public AvaliacaoCommandHandler(IAvaliacaoRepository avaliacaoRepository,
                                    INotificationContext notificationContext,
                                    IMediator mediator) : base(mediator)
        {
            this.avaliacaoRepository = avaliacaoRepository;
            this.notificationContext = notificationContext;
        }

        public async Task<Guid> Handle(CadastrarAvaliacaoCommand request, CancellationToken cancellationToken)
        {
            var avaliacao = new Avaliacao(request.Nota, request.UsuarioId, request.FilmeId);

            if (!avaliacao.Valido)
            {
                notificationContext.AddNotifications(avaliacao.ValidationResult);
                return Guid.Empty;
            }
            
            avaliacaoRepository.Inserir(avaliacao);
            await avaliacaoRepository.Salvar();

            return avaliacao.Id;
        }

    }
}