using System;
using System.Threading;
using System.Threading.Tasks;
using Desafio.Application.Commands;
using Desafio.Application.Handlers.Base;
using Desafio.CrossCutting.Notification;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using MediatR;

namespace Desafio.Application.Handlers
{
    public class UsuarioCommandHandler : CommandHandler,
        IRequestHandler<CadastrarUsuarioCommand, Guid>,
        IRequestHandler<AtivarUsuarioCommand, Unit>,
        IRequestHandler<DesativarUsuarioCommand, Unit>,
        IRequestHandler<EditarUsuarioCommand, Unit>,
        IRequestHandler<LogarUsuarioCommand, Usuario>
    {
        private readonly IUsuarioRepository usuarioRepository;
        private readonly INotificationContext notificationContext;
        public UsuarioCommandHandler(IUsuarioRepository usuarioRepository,
                                    INotificationContext notificationContext,
                                    IMediator mediator) : base(mediator)
        {
            this.usuarioRepository = usuarioRepository;
            this.notificationContext = notificationContext;
        }

        public async Task<Guid> Handle(CadastrarUsuarioCommand request, CancellationToken cancellationToken)
        {
            var senha = BCrypt.Net.BCrypt.HashPassword(request.Senha);
            var usuario = new Usuario(request.Nome, request.Login, senha, request.Administrador);

            if (!usuario.Valido)
            {
                notificationContext.AddNotifications(usuario.ValidationResult);
                return Guid.Empty;
            }
            usuarioRepository.Inserir(usuario);
            await usuarioRepository.Salvar();

            return usuario.Id;
        }

        public async Task<Unit> Handle(AtivarUsuarioCommand request, CancellationToken cancellationToken)
        {
            var usuario = await usuarioRepository.ObterPorId(request.Id);

            if (usuario == null)
            {
                notificationContext.AddNotification("usuario", "Usuário não encontrado");
                return Unit.Value;
            }

            usuario.Ativar();
            usuarioRepository.Atualizar(usuario);
            await usuarioRepository.Salvar();

            return Unit.Value;
        }

        public async Task<Unit> Handle(DesativarUsuarioCommand request, CancellationToken cancellationToken)
        {
            var usuario = await usuarioRepository.ObterPorId(request.Id);

            if (usuario == null)
            {
                notificationContext.AddNotification("usuario", "Usuário não encontrado");
                return Unit.Value;
            }

            usuario.Desativar();
            usuarioRepository.Atualizar(usuario);
            await usuarioRepository.Salvar();

            return Unit.Value;
        }

        public async Task<Unit> Handle(EditarUsuarioCommand request, CancellationToken cancellationToken)
        {
            var usuario = await usuarioRepository.ObterPorId(request.Id);

            if (usuario == null)
            {
                notificationContext.AddNotification("usuario", "Usuário não encontrado");
                return Unit.Value;
            }

            usuario.AtualizarDados(request.Nome, request.Login);
            usuarioRepository.Atualizar(usuario);
            await usuarioRepository.Salvar();

            return Unit.Value;
        }

        public async Task<Usuario> Handle(LogarUsuarioCommand request, CancellationToken cancellationToken)
        {
            var usuario = await usuarioRepository.ObterPorLogin(request.Login);

            if (usuario == null || !BCrypt.Net.BCrypt.Verify(request.Senha, usuario.Senha))
            {
                notificationContext.AddNotification("usuario", "Login ou senha inválidos");
                return null;
            }

            return usuario;
        }

    }
}