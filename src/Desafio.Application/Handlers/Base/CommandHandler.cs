using MediatR;

namespace Desafio.Application.Handlers.Base
{
    public class CommandHandler
    {
         protected readonly IMediator Mediator;
        
        public CommandHandler(IMediator mediator)
            => (Mediator) = (mediator);
    }
}