using System;
using System.Threading;
using System.Threading.Tasks;
using Desafio.Application.Commands;
using Desafio.Application.Handlers.Base;
using Desafio.CrossCutting.Notification;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using MediatR;

namespace Desafio.Application.Handlers
{
    public class FilmeCommandHandler : CommandHandler,
        IRequestHandler<CadastrarFilmeCommand, Guid>
    {
        private readonly IFilmeRepository filmeRepository;
        private readonly INotificationContext notificationContext;
        public FilmeCommandHandler(IFilmeRepository filmeRepository,
                                    INotificationContext notificationContext,
                                    IMediator mediator) : base(mediator)
        {
            this.filmeRepository = filmeRepository;
            this.notificationContext = notificationContext;
        }

        public async Task<Guid> Handle(CadastrarFilmeCommand request, CancellationToken cancellationToken)
        {
            var filme = new Filme(request.Nome, request.Descricao, request.Ano);

            if (!filme.Valido)
            {
                notificationContext.AddNotifications(filme.ValidationResult);
                return Guid.Empty;
            }

            filme.AdicionarDiretores(request.DiretoresIds);
            filme.AdicionarAtores(request.AtoresIds);
            
            filmeRepository.Inserir(filme);
            await filmeRepository.Salvar();

            return filme.Id;
        }

    }
}