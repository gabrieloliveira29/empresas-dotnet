﻿using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;

namespace Desafio.Application.Queries.Interfaces
{
    public interface IDiretorQuery
    {
        PagedList<Diretor> Listar(int pagina, int porPagina);
    }
}
