﻿using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;

namespace Desafio.Application.Queries.Interfaces
{
    public interface IUsuarioQuery
    {
        PagedList<Usuario> Listar(int pagina, int porPagina);
    }
}
