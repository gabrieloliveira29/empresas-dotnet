﻿using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;

namespace Desafio.Application.Queries.Interfaces
{
    public interface IAtorQuery
    {
        PagedList<Ator> Listar(int pagina, int porPagina);
    }
}
