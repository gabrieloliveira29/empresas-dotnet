﻿using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;

namespace Desafio.Application.Queries.Interfaces
{
    public interface IGeneroQuery
    {
        PagedList<Genero> Listar(int pagina, int porPagina);
    }
}
