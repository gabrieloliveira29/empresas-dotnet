﻿using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;

namespace Desafio.Application.Queries.Interfaces
{
    public interface IFilmeQuery
    {
        PagedList<Filme> Listar(int pagina, int porPagina);
    }
}
