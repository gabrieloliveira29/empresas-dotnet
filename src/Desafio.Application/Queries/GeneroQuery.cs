﻿using Desafio.Application.Queries.Interfaces;
using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;

namespace Desafio.Application.Queries
{
    public class GeneroQuery : IGeneroQuery
    {
        private readonly IGeneroRepository generoRepository;
        public GeneroQuery(IGeneroRepository generoRepository)
        {
            this.generoRepository = generoRepository;
        }

        public PagedList<Genero> Listar(int pagina, int porPagina)
        {
            return generoRepository.Listar(pagina, porPagina);
        }
    }
}
