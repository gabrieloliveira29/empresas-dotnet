﻿using Desafio.Application.Queries.Interfaces;
using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;

namespace Desafio.Application.Queries
{
    public class FilmeQuery : IFilmeQuery
    {
        private readonly IFilmeRepository filmeRepository;
        public FilmeQuery(IFilmeRepository filmeRepository)
        {
            this.filmeRepository = filmeRepository;
        }

        public PagedList<Filme> Listar(int pagina, int porPagina)
        {
            return filmeRepository.Listar(pagina, porPagina);
        }
    }
}
