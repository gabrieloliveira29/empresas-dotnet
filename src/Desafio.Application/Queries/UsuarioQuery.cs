﻿using Desafio.Application.Queries.Interfaces;
using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;

namespace Desafio.Application.Queries
{
    public class UsuarioQuery : IUsuarioQuery
    {
        private readonly IUsuarioRepository usuarioRepository;
        public UsuarioQuery(IUsuarioRepository usuarioRepository)
        {
            this.usuarioRepository = usuarioRepository;
        }

        public PagedList<Usuario> Listar(int pagina, int porPagina)
        {
            return usuarioRepository.Listar(pagina, porPagina);
        }
    }
}
