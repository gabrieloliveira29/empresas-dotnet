﻿using Desafio.Application.Queries.Interfaces;
using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;

namespace Desafio.Application.Queries
{
    public class AtorQuery : IAtorQuery
    {
        private readonly IAtorRepository atorRepository;
        public AtorQuery(IAtorRepository atorRepository)
        {
            this.atorRepository = atorRepository;
        }

        public PagedList<Ator> Listar(int pagina, int porPagina)
        {
            return atorRepository.Listar(pagina, porPagina);
        }
    }
}
