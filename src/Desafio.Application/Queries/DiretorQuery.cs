﻿using Desafio.Application.Queries.Interfaces;
using Desafio.CrossCutting.Pagination;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;

namespace Desafio.Application.Queries
{
    public class DiretorQuery : IDiretorQuery
    {
        private readonly IDiretorRepository diretorRepository;
        public DiretorQuery(IDiretorRepository diretorRepository)
        {
            this.diretorRepository = diretorRepository;
        }

        public PagedList<Diretor> Listar(int pagina, int porPagina)
        {
            return diretorRepository.Listar(pagina, porPagina);
        }
    }
}
