using System;
using MediatR;

namespace Desafio.Application.Commands
{
    public class AtivarUsuarioCommand : IRequest
    {
        public AtivarUsuarioCommand(Guid id) => (Id) = (id);

       public Guid Id {get; private set;}
    }
}