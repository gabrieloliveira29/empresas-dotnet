using System;
using MediatR;

namespace Desafio.Application.Commands
{
    public class CadastrarDiretorCommand : IRequest<Guid>
    {
        public CadastrarDiretorCommand(string nome, string sobrenome) => (Nome, Sobrenome) = (nome, sobrenome);

        public string Nome { get; private set; }
        public string Sobrenome { get; private set; }
    }
}