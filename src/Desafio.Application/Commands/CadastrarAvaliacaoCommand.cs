using System;
using MediatR;

namespace Desafio.Application.Commands
{
    public class CadastrarAvaliacaoCommand : IRequest<Guid>
    {
        public CadastrarAvaliacaoCommand(int nota, Guid usuarioId, Guid filmeId) => (Nota, UsuarioId, FilmeId) = (nota, usuarioId, filmeId);

        public int Nota { get; private set; }
        public Guid UsuarioId { get; private set; }
        public Guid FilmeId { get; private set; }
    }
}