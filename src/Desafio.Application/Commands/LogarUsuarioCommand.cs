using System;
using Desafio.Domain.Entities;
using MediatR;

namespace Desafio.Application.Commands
{
    public class LogarUsuarioCommand : IRequest<Usuario>
    {
        public LogarUsuarioCommand(string login, string senha)
        => (Login, Senha) = (login, senha);

        public string Login { get; private set; }
        public string Senha { get; private set; }
    }
}