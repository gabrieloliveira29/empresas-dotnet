using System;
using MediatR;

namespace Desafio.Application.Commands
{
    public class EditarUsuarioCommand : IRequest
    {
        public EditarUsuarioCommand(Guid id, string nome, string login)
        => (Id, Nome, Login) = (id, nome, login);

        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public string Login { get; private set; }
    }
}