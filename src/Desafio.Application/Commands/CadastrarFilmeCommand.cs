using System;
using System.Collections.Generic;
using MediatR;

namespace Desafio.Application.Commands
{
    public class CadastrarFilmeCommand : IRequest<Guid>
    {
        public CadastrarFilmeCommand(string nome, string descricao, int ano, IEnumerable<Guid> diretoresIds, IEnumerable<Guid> atoresIds)
        => (Nome, Descricao, Ano, DiretoresIds, AtoresIds) = (nome, descricao, ano, diretoresIds, atoresIds);

        public string Nome { get; private set; }
        public string Descricao { get; private set; }
        public int Ano { get; private set; }
        public IEnumerable<Guid> DiretoresIds { get; private set; }
        public IEnumerable<Guid> AtoresIds { get; private set; }
    }
}