using System;
using MediatR;

namespace Desafio.Application.Commands
{
    public class DesativarUsuarioCommand : IRequest
    {
        public DesativarUsuarioCommand(Guid id) => (Id) = (id);

       public Guid Id {get; private set;}
    }
}