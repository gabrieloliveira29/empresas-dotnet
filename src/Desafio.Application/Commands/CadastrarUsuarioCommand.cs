using System;
using MediatR;

namespace Desafio.Application.Commands
{
    public class CadastrarUsuarioCommand : IRequest<Guid>
    {
        public CadastrarUsuarioCommand(string nome, string login, string senha, bool administrador)
        => (Nome, Login, Senha, Administrador) = (nome, login, senha, administrador);

        public string Nome { get; private set; }
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public bool Administrador { get; private set; }
    }
}