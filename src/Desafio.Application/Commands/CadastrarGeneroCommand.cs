using System;
using MediatR;

namespace Desafio.Application.Commands
{
    public class CadastrarGeneroCommand : IRequest<Guid>
    {
        public CadastrarGeneroCommand(string descricao) => (Descricao) = (descricao);

        public string Descricao { get; private set; }
    }
}