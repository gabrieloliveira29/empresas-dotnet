using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class FilmeGeneroMap : IEntityTypeConfiguration<FilmeGenero>
    {
        public void Configure(EntityTypeBuilder<FilmeGenero> entity)
        {
            entity.ToTable("tb_filme_genero");

            entity.HasKey(filmeGenero => new { filmeGenero.GeneroId, filmeGenero.FilmeId });

            entity.Property(filmeGenero => filmeGenero.GeneroId)
                .HasColumnName("cd_genero");

            entity.Property(filmeAtor => filmeAtor.FilmeId)
                .HasColumnName("cd_filme");

            entity.HasOne(filmeGenero => filmeGenero.Filme)
                .WithMany(filme => filme.Generos)
                .HasForeignKey(filmeGenero => filmeGenero.FilmeId);
            
            entity.HasOne(filmeGenero => filmeGenero.Genero)
                .WithMany(genero => genero.Filmes)
                .HasForeignKey(filmeGenero => filmeGenero.GeneroId);
        }
    }
}