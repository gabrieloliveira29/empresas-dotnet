using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class DiretorMap : IEntityTypeConfiguration<Diretor>
    {
        public void Configure(EntityTypeBuilder<Diretor> entity)
        {
            entity.ToTable("tb_diretor");

            entity.HasKey(diretor => diretor.Id);

            entity.Property(diretor => diretor.Id)
                .HasColumnName("cd_diretor");

            entity.Property(diretor => diretor.Nome)
                .IsRequired()
                .HasColumnName("nm_diretor");
            
            entity.Property(diretor => diretor.Sobrenome)
                .IsRequired()
                .HasColumnName("sn_diretor");
            
             entity.Ignore(ator => ator.Valido);
            entity.Ignore(ator => ator.ValidationResult);
        }
    }
}