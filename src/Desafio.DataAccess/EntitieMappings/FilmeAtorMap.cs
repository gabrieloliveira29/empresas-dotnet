using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class FilmeAtorMap : IEntityTypeConfiguration<FilmeAtor>
    {
        public void Configure(EntityTypeBuilder<FilmeAtor> entity)
        {
            entity.ToTable("tb_filme_ator");

            entity.HasKey(filmeAtor => new { filmeAtor.AtorId, filmeAtor.FilmeId });

            entity.Property(filmeAtor => filmeAtor.AtorId)
                .HasColumnName("cd_ator");

            entity.Property(filmeAtor => filmeAtor.FilmeId)
                .HasColumnName("cd_filme");

            entity.HasOne(filmeAtor => filmeAtor.Filme)
                .WithMany(filme => filme.Atores)
                .HasForeignKey(filmeAtor => filmeAtor.FilmeId);
            
            entity.HasOne(filmeAtor => filmeAtor.Ator)
                .WithMany(ator => ator.Filmes)
                .HasForeignKey(filmeAtor => filmeAtor.AtorId);
            
        }
    }
}