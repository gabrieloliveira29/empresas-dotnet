using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class AvaliacaoMap : IEntityTypeConfiguration<Avaliacao>
    {
        public void Configure(EntityTypeBuilder<Avaliacao> entity)
        {
            entity.ToTable("tb_avaliacao");

            entity.HasKey(avaliacao => avaliacao.Id);

            entity.Property(avaliacao => avaliacao.Id)
                .HasColumnName("cd_avaliacao");

            entity.Property(avaliacao => avaliacao.Nota)
                .IsRequired()
                .HasColumnName("nr_nota");

            entity.HasCheckConstraint("chk_avaliacao", "nr_nota >= 0 AND nr_nota <= 4");

            entity.Property(avaliacao => avaliacao.FilmeId)
                .IsRequired()
                .HasColumnName("cd_filme");

            entity.Property(avaliacao => avaliacao.UsuarioId)
                .IsRequired()
                .HasColumnName("cd_usuario");

            entity.HasOne(avaliacao => avaliacao.Usuario)
                .WithMany(usuario => usuario.Avaliacoes)
                .HasForeignKey(avaliacao => avaliacao.UsuarioId);

            entity.HasOne(avaliacao => avaliacao.Filme)
                .WithMany(filme => filme.Avaliacoes)
                .HasForeignKey(avaliacao => avaliacao.FilmeId);

            entity.Ignore(ator => ator.Valido);
            entity.Ignore(ator => ator.ValidationResult);
        }
    }
}