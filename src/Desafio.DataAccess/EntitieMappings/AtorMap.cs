using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class AtorMap : IEntityTypeConfiguration<Ator>
    {
        public void Configure(EntityTypeBuilder<Ator> entity)
        {
            entity.ToTable("tb_ator");

            entity.HasKey(ator => ator.Id);

            entity.Property(ator => ator.Id)
                .HasColumnName("cd_ator");

            entity.Property(ator => ator.Nome)
                .IsRequired()
                .HasColumnName("nm_ator");
            
            entity.Property(ator => ator.Sobrenome)
                .IsRequired()
                .HasColumnName("sn_ator");

            entity.Ignore(ator => ator.Valido);
            entity.Ignore(ator => ator.ValidationResult);
            
        }
    }
}