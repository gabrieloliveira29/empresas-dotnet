using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class GeneroMap : IEntityTypeConfiguration<Genero>
    {
        public void Configure(EntityTypeBuilder<Genero> entity)
        {
            entity.ToTable("tb_genero");

            entity.HasKey(genero => genero.Id);

            entity.Property(genero => genero.Id)
                .HasColumnName("cd_genero");

            entity.Property(genero => genero.Descricao)
                .IsRequired()
                .HasColumnName("ds_genero");
            
             entity.Ignore(ator => ator.Valido);
            entity.Ignore(ator => ator.ValidationResult);
        }
    }
}