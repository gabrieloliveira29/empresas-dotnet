using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class FilmeDiretorMap : IEntityTypeConfiguration<FilmeDiretor>
    {
        public void Configure(EntityTypeBuilder<FilmeDiretor> entity)
        {
            entity.ToTable("tb_filme_diretor");

            entity.HasKey(filmeDiretor => new { filmeDiretor.DiretorId, filmeDiretor.FilmeId });

            entity.Property(filmeDiretor => filmeDiretor.DiretorId)
                .HasColumnName("cd_diretor");

            entity.Property(filmeDiretor => filmeDiretor.FilmeId)
                .HasColumnName("cd_filme");

            entity.HasOne(filmeDiretor => filmeDiretor.Filme)
                .WithMany(filme => filme.Diretores)
                .HasForeignKey(filmeDiretor => filmeDiretor.FilmeId);
            
            entity.HasOne(filmeDiretor => filmeDiretor.Diretor)
                .WithMany(diretor => diretor.Filmes)
                .HasForeignKey(filmeDiretor => filmeDiretor.DiretorId);
        }
    }
}