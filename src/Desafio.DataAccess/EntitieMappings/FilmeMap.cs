using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class FilmeMap : IEntityTypeConfiguration<Filme>
    {
        public void Configure(EntityTypeBuilder<Filme> entity)
        {
            entity.ToTable("tb_filme");

            entity.HasKey(filme => filme.Id);

            entity.Property(filme => filme.Id)
                .HasColumnName("cd_filme");

            entity.Property(filme => filme.Nome)
                .IsRequired()
                .HasColumnName("nm_filme");

            entity.Property(filme => filme.Descricao)
                .IsRequired()
                .HasColumnName("ds_filme");
            
            entity.Property(filme => filme.Ano)
                .IsRequired()
                .HasColumnName("nr_ano");
                
             entity.Ignore(ator => ator.Valido);
            entity.Ignore(ator => ator.ValidationResult);
        }
    }
}