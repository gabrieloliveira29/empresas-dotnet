using Desafio.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Desafio.DataAccess.EntitieMappings
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> entity)
        {
            entity.ToTable("tb_usuario");

            entity.HasKey(usuario => usuario.Id);

            entity.Property(usuario => usuario.Id)
                .HasColumnName("cd_usuario");

            entity.Property(usuario => usuario.Nome)
                .IsRequired()
                .HasColumnName("nm_usuario");

            entity.Property(usuario => usuario.Login)
                .IsRequired()
                .HasColumnName("ds_login");

            entity.Property(usuario => usuario.Cadastro)
                .IsRequired()
                .HasColumnName("dt_cadastro");

            entity.Property(usuario => usuario.Ativo)
                .HasColumnName("fl_ativo");

            entity.Property(usuario => usuario.Administrador)
                .HasColumnName("fl_administrador");

            entity.Property(usuario => usuario.Senha)
                .IsRequired()
                .HasColumnName("ds_senha");
            
             entity.Ignore(ator => ator.Valido);
            entity.Ignore(ator => ator.ValidationResult);
        }
    }
}