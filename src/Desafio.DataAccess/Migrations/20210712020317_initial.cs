﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Desafio.DataAccess.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tb_ator",
                columns: table => new
                {
                    cd_ator = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    nm_ator = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    sn_ator = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_ator", x => x.cd_ator);
                });

            migrationBuilder.CreateTable(
                name: "tb_diretor",
                columns: table => new
                {
                    cd_diretor = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    nm_diretor = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    sn_diretor = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_diretor", x => x.cd_diretor);
                });

            migrationBuilder.CreateTable(
                name: "tb_filme",
                columns: table => new
                {
                    cd_filme = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    nm_filme = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ds_filme = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    nr_ano = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_filme", x => x.cd_filme);
                });

            migrationBuilder.CreateTable(
                name: "tb_genero",
                columns: table => new
                {
                    cd_genero = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ds_genero = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_genero", x => x.cd_genero);
                });

            migrationBuilder.CreateTable(
                name: "tb_usuario",
                columns: table => new
                {
                    cd_usuario = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    nm_usuario = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ds_login = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ds_senha = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    fl_ativo = table.Column<bool>(type: "bit", nullable: false),
                    fl_administrador = table.Column<bool>(type: "bit", nullable: false),
                    dt_cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_usuario", x => x.cd_usuario);
                });

            migrationBuilder.CreateTable(
                name: "tb_filme_ator",
                columns: table => new
                {
                    cd_filme = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    cd_ator = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_filme_ator", x => new { x.cd_ator, x.cd_filme });
                    table.ForeignKey(
                        name: "FK_tb_filme_ator_tb_ator_cd_ator",
                        column: x => x.cd_ator,
                        principalTable: "tb_ator",
                        principalColumn: "cd_ator",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tb_filme_ator_tb_filme_cd_filme",
                        column: x => x.cd_filme,
                        principalTable: "tb_filme",
                        principalColumn: "cd_filme",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tb_filme_diretor",
                columns: table => new
                {
                    cd_filme = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    cd_diretor = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_filme_diretor", x => new { x.cd_diretor, x.cd_filme });
                    table.ForeignKey(
                        name: "FK_tb_filme_diretor_tb_diretor_cd_diretor",
                        column: x => x.cd_diretor,
                        principalTable: "tb_diretor",
                        principalColumn: "cd_diretor",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tb_filme_diretor_tb_filme_cd_filme",
                        column: x => x.cd_filme,
                        principalTable: "tb_filme",
                        principalColumn: "cd_filme",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tb_filme_genero",
                columns: table => new
                {
                    cd_filme = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    cd_genero = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_filme_genero", x => new { x.cd_genero, x.cd_filme });
                    table.ForeignKey(
                        name: "FK_tb_filme_genero_tb_filme_cd_filme",
                        column: x => x.cd_filme,
                        principalTable: "tb_filme",
                        principalColumn: "cd_filme",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tb_filme_genero_tb_genero_cd_genero",
                        column: x => x.cd_genero,
                        principalTable: "tb_genero",
                        principalColumn: "cd_genero",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tb_avaliacao",
                columns: table => new
                {
                    cd_avaliacao = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    nr_nota = table.Column<int>(type: "int", nullable: false),
                    cd_usuario = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    cd_filme = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_avaliacao", x => x.cd_avaliacao);
                    table.CheckConstraint("chk_avaliacao", "nr_nota >= 1 AND nr_nota <= 10");
                    table.ForeignKey(
                        name: "FK_tb_avaliacao_tb_filme_cd_filme",
                        column: x => x.cd_filme,
                        principalTable: "tb_filme",
                        principalColumn: "cd_filme",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tb_avaliacao_tb_usuario_cd_usuario",
                        column: x => x.cd_usuario,
                        principalTable: "tb_usuario",
                        principalColumn: "cd_usuario",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tb_avaliacao_cd_filme",
                table: "tb_avaliacao",
                column: "cd_filme");

            migrationBuilder.CreateIndex(
                name: "IX_tb_avaliacao_cd_usuario",
                table: "tb_avaliacao",
                column: "cd_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_tb_filme_ator_cd_filme",
                table: "tb_filme_ator",
                column: "cd_filme");

            migrationBuilder.CreateIndex(
                name: "IX_tb_filme_diretor_cd_filme",
                table: "tb_filme_diretor",
                column: "cd_filme");

            migrationBuilder.CreateIndex(
                name: "IX_tb_filme_genero_cd_filme",
                table: "tb_filme_genero",
                column: "cd_filme");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tb_avaliacao");

            migrationBuilder.DropTable(
                name: "tb_filme_ator");

            migrationBuilder.DropTable(
                name: "tb_filme_diretor");

            migrationBuilder.DropTable(
                name: "tb_filme_genero");

            migrationBuilder.DropTable(
                name: "tb_usuario");

            migrationBuilder.DropTable(
                name: "tb_ator");

            migrationBuilder.DropTable(
                name: "tb_diretor");

            migrationBuilder.DropTable(
                name: "tb_filme");

            migrationBuilder.DropTable(
                name: "tb_genero");
        }
    }
}
