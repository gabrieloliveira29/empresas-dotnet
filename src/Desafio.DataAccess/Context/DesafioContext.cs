using Microsoft.EntityFrameworkCore;

namespace Desafio.DataAccess.Context
{
    public class DesafioContext : DbContext
    {
        public DesafioContext() { }
        public DesafioContext(DbContextOptions<DesafioContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder DbContextOptionsBuilder)
        {
            DbContextOptionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DesafioContext).Assembly);
        }
    }
}