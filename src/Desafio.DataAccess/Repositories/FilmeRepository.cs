using Desafio.CrossCutting.Pagination;
using Desafio.DataAccess.Context;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using Desafio.Domain.Interfaces.Repositories.Base;
using System.Linq;

namespace Desafio.DataAccess.Repositories
{
    public class FilmeRepository : RepositoryBase<Filme>, IFilmeRepository
    {
        public FilmeRepository(DesafioContext context) : base(context)
        {
        }

        public override PagedList<Filme> Listar(int pagina, int porPagina)
        {
            return context.Set<Filme>().OrderBy(filme => filme.NotaAvaliacao)
                                       .ThenBy(filme => filme.Nome)
                                       .ToPagedList(pagina, porPagina);
        }
    }
}