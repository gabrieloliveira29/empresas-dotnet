using Desafio.DataAccess.Context;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using Desafio.Domain.Interfaces.Repositories.Base;

namespace Desafio.DataAccess.Repositories
{
    public class DiretorRepository : RepositoryBase<Diretor>, IDiretorRepository
    {
        public DiretorRepository(DesafioContext context) : base(context)
        {
        }
    }
}