using Desafio.DataAccess.Context;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using Desafio.Domain.Interfaces.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Desafio.DataAccess.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(DesafioContext context) : base(context)
        {
        }

        public Task<Usuario> ObterPorLogin(string login)
        {
            return context.Set<Usuario>().FirstOrDefaultAsync(usuario => usuario.Login.Equals(login));
        }
    }
}