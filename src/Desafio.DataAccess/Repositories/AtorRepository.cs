using Desafio.DataAccess.Context;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using Desafio.Domain.Interfaces.Repositories.Base;

namespace Desafio.DataAccess.Repositories
{
    public class AtorRepository : RepositoryBase<Ator>, IAtorRepository
    {
        public AtorRepository(DesafioContext context) : base(context)
        {
        }
    }
}