using Desafio.CrossCutting.Pagination;
using Desafio.DataAccess.Context;
using Desafio.Domain.Entities;
using Desafio.Domain.Interfaces.Repositories;
using Desafio.Domain.Interfaces.Repositories.Base;
using System.Linq;

namespace Desafio.DataAccess.Repositories
{
    public class GeneroRepository : RepositoryBase<Genero>, IGeneroRepository
    {
        public GeneroRepository(DesafioContext context) : base(context)
        {
        }

        public PagedList<Genero> ListarTodos()
        {
            return context.Set<Genero>().ToPagedList(10,10);
        }
    }
}