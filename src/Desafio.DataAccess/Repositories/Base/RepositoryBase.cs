using System;
using System.Threading.Tasks;
using Desafio.CrossCutting.Pagination;
using Desafio.DataAccess.Context;
using Desafio.Domain.Entities.Abstracts;

namespace Desafio.Domain.Interfaces.Repositories.Base
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : Entity
    {
        protected DesafioContext context;
        public RepositoryBase(DesafioContext context)
        {
            this.context = context;
        }

        public virtual void Atualizar(TEntity entidade)
        {
            context.Set<TEntity>().Update(entidade);
        }

        public virtual void Inserir(TEntity entidade)
        {
            context.Set<TEntity>().Add(entidade);
        }

        public virtual async Task<TEntity> ObterPorId(Guid id)
        {
            return await context.Set<TEntity>().FindAsync(id);
        }

        public virtual PagedList<TEntity> Listar(int pagina, int porPagina)
        {
            return context.Set<TEntity>().ToPagedList(pagina, porPagina);
        }

        public virtual void Remover(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public virtual async void Remover(Guid id)
        {
            Remover(await ObterPorId(id));
        }

        public async Task<bool> Salvar()
        {
            return await context.SaveChangesAsync() > 0;
        }
    }
}